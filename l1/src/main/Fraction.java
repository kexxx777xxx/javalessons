package main;

public class Fraction {
    int number;
    int denominator;

    public Fraction divide(Fraction f) {
        this.multiply(f.denominator, f.number);
        this.round();

        return this;
    }

    public Fraction add(Fraction f) {
        this.sum(f.number, f.denominator);
        this.round();

        return this;
    }

    private void round() {
        this.round(0);
    }

    int[] values = {10, 7, 5, 3, 2};

    private void round(int i) {
        int value;

        if (values.length <= i) {
            for (int j = this.number <= this.denominator ? this.number : this.denominator; j > 1; j--) {
                if (this.number % i == 0 && this.denominator % i == 0) {
                    this.number /= i;
                    this.denominator /= i;
                    break;
                }
            }
            return;
        } else {
            value = values[i];
        }

        if (number % value == 0 && denominator % value == 0) {
            number /= value;
            denominator /= value;
        } else {
            i++;
        }

        this.round(i);
    }

    public Fraction multiply(Fraction f) {
        this.multiply(f.number, f.denominator);
        this.round();

        return this;
    }

    public Fraction(int number, int denominator) {
        this.number = number;
        this.denominator = denominator;
    }

    public Fraction subtract(Fraction f) {
        this.sum(f.number * -1, f.denominator);
        this.round();

        return this;
    }

    private void sum(int number, int denominator) {
        int n1 = this.number, n2 = number, d1 = this.denominator, d2 = denominator;

        if (d1 != d2) {
            n1 *= d2;
            n2 *= d1;
            d1 *= d2;
        }

        this.number = n1 + n2;
        this.denominator = d1;
    }

    private void multiply(int number, int denominator) {
        this.number *= number;
        this.denominator *= denominator;
    }

    public void show() {
        String str = "";
        if (this.number > this.denominator) {
            int wn = this.number / this.denominator;
            str += wn + " ";
            str += this.number - this.denominator * wn;
            str += "/" + this.denominator;
        }

        System.out.println(str);
    }

    public int getNumber() {
        return number;
    }

    public int getDenominator() {
        return denominator;
    }
}