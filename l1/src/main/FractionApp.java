package main;

public class FractionApp {
    public static void main(String[] args) {
        for (int i = 100, j = 200; i < 300; i++, j--) {
            int n1 = i, n2 = j, n3 = j, n4 = i;
            System.out.print(n1 + "/" + n2 + " + " + n3 + "/" + n4 + " ");
            new Fraction(n1, n2).add(new Fraction(n3, n4)).show();
            System.out.print(n1 + "/" + n2 + " - " + n3 + "/" + n4 + " ");
            new Fraction(n1, n2).subtract(new Fraction(n3, n4)).show();
            System.out.print(n1 + "/" + n2 + " * " + n3 + "/" + n4 + " ");
            new Fraction(n1, n2).multiply(new Fraction(n3, n4)).show();
            System.out.print(n1 + "/" + n2 + " / " + n3 + "/" + n4 + " ");
            new Fraction(n1, n2).divide(new Fraction(n3, n4)).show();
        }
    }
}
