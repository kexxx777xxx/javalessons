package main;

class Student {
    String name;
    int age;
    String group;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void sayName() {
        System.out.println("Hello, my name is " + getName());
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

public class app {
    public static void main(String[] args) {
        Student student1 = new Student("Vasa1", 1);
        Student student2 = new Student("Vasa2", 11);

        student1.sayName();
        student2.sayName();
    }
}
