package test.java;

import main.Calculator;
import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by kex1 on 4/1/15.
 */
public class CalculatorTest {
    Calculator calc;

    @Before
    public void init() {
        calc = new Calculator();
    }

    @Test
    public void shouldAddTwoNumbers() {
        assertEquals(3, calc.add(1, 2));
    }

    @Test
    public void vasa() {
        assertEquals(7, calc.sub(14, 2));
    }

    @Test
    public void testMinusovie() {
        assertEquals(-2, calc.add(0, -2));
    }
}
