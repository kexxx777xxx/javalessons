package test.java;

import main.Fraction;
import main.FractionApp;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FractionTest {
    Fraction f1;
    Fraction f2;

    @Before
    public void init() {
        Fraction f1 = new Fraction(10, 11);
        Fraction f2 = new Fraction(9, 12);
    }

    @Test
    public void testDivide() throws Exception {
        Fraction f1 = new Fraction(10, 11);
        Fraction f2 = new Fraction(9, 12);

        f1.divide(f2);

        assertEquals(40, f1.getNumber());
        assertEquals(33, f1.getDenominator());
    }

    @Test
    public void testAdd() throws Exception {
        Fraction f1 = new Fraction(10, 11);
        Fraction f2 = new Fraction(9, 12);

        f1.add(f2);

        assertEquals(73, f1.getNumber());
        assertEquals(44, f1.getDenominator());

    }

    @Test
    public void testMultiply() throws Exception {
        Fraction f1 = new Fraction(10, 11);
        Fraction f2 = new Fraction(9, 12);

        f1.multiply(f2);

        assertEquals(15, f1.getNumber());
        assertEquals(22, f1.getDenominator());
    }

    @Test
    public void testSubtract() throws Exception {
        Fraction f1 = new Fraction(10, 11);
        Fraction f2 = new Fraction(9, 12);

        f1.subtract(f2);

        assertEquals(7, f1.getNumber());
        assertEquals(44, f1.getDenominator());
    }

    @Test
    public void testSubstractConfigured(){
        Fraction f1 = new Fraction(295, 5);
        Fraction f2 = new Fraction(5, 295);

        f1.subtract(f2);

        assertEquals(3480, f1.getNumber());
        assertEquals(59, f1.getDenominator());
    }

    @Test
    public void testShow() throws Exception {

    }
}
